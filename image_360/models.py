from django.db import models
from django.utils.text import slugify


class Category(models.Model):
    name = models.CharField(max_length=40)
    icon = models.ImageField(upload_to="icons")
    slug = models.SlugField(max_length=40, unique=True)

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = self.slug or slugify(self.name)
        super().save(*args, **kwargs)


class Image360(models.Model):
    image360 = models.ImageField(upload_to="pics_360", blank=True, null=True)
    image = models.ImageField(upload_to="images", blank=True, null=True)
    description = models.TextField()
    category = models.ForeignKey("Category", on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Images360"

    def __str__(self):
        return self.description
