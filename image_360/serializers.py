from rest_framework import serializers

from .models import Image360, Category


class Image360Serializer(serializers.ModelSerializer):
    class Meta:
        model = Image360
        fields = "__all__"


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"
