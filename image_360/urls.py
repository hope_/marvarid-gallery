from django.urls import path
from . import views
app_name = "image_360"

urlpatterns = [
    path("<slug:slug>/all/", views.Image360List.as_view(), name="image-get"),
    path("categories/", views.CategoryList.as_view(), name="category"),
    path("", views.ImageSearch.as_view(), name="image-search")

]
