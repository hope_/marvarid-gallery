from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Image360, Category
from .serializers import Image360Serializer, CategorySerializer


class Image360List(APIView):

    def get(self, request, slug):
        images = Image360.objects.filter(category__slug=slug)
        serializer = Image360Serializer(images, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = Image360Serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CategoryList(APIView):

    def get(self, request):
        categories = Category.objects.all()
        serializer = CategorySerializer(categories, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = CategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ImageSearch(APIView):

    def get(self, request):
        term = request.GET['term']
        images = Image360.objects.filter(description__contains=term)
        serializer = Image360Serializer(images, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
