from django.contrib import admin

from .models import Category, Image360


class CategoryAdmin(admin.ModelAdmin):
    list_display = ["name"]
    prepopulated_fields = {"slug": ("name",)}


class Image360Admin(admin.ModelAdmin):
    list_display = ["description"]


admin.site.register(Category, CategoryAdmin)
admin.site.register(Image360, Image360Admin)
